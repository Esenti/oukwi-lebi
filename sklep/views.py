from django.http import HttpResponse
from django.template import RequestContext, loader
from django.contrib.auth import authenticate
from sklep.models import Kategoria, Produkt, Koszyk, ProduktKoszykowy
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django import forms
from django.core.urlresolvers import reverse

def index(request):
	template = loader.get_template('sklep/index.html')
	context = RequestContext(request)
	return HttpResponse(template.render(context))


def kojot(request):
	return HttpResponse("Tutaj kojot pszemek.")


def parsuj_kategorie(rodzic, wynik):
	for kat in rodzic.kategoria_set.all():
		wynik += kat.produkt_set.all()
		parsuj_kategorie(kat, wynik)


def kategoria(request, requested_id):

	wybranaKategoria = Kategoria.objects.get(id=requested_id)
	produkt_list = list(wybranaKategoria.produkt_set.order_by('nazwa_prod'))

	parsuj_kategorie(wybranaKategoria, produkt_list)

	template = loader.get_template('sklep/produkty.html')
	context = RequestContext(request, {
		'produkt_list': produkt_list,
		'wybranaKategoria' : wybranaKategoria,
	})
	return HttpResponse(template.render(context))

def produkt(request, requested_id):
	prod = Produkt.objects.get(id=requested_id)

	template = loader.get_template('sklep/detail.html')
	context = RequestContext(request, {
		'prod' : prod,
	})
	return HttpResponse(template.render(context))

def kontakt(request):

	template = loader.get_template('sklep/kontakt.html')
	context = RequestContext(request)
	return HttpResponse(template.render(context))

def osklepie(request):

	template = loader.get_template('sklep/osklepie.html')
	context = RequestContext(request)
	return HttpResponse(template.render(context))

def zaloguj(request):


	if request.method == 'POST':
		form = AuthenticationForm(data=request.POST)

		if form.is_valid():
			login(request, form.get_user())
			return redirect('index')
		else:
			pass
	else:
		form = AuthenticationForm()


	template = loader.get_template('sklep/zaloguj.html')
	context = RequestContext(request, {
		'form': form
	})

	return HttpResponse(template.render(context))

def nowekonto(request):

	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			new_user = form.save()
			return HttpResponseRedirect('/sklep')
	else:
		form = UserCreationForm()


	template = loader.get_template('sklep/nowekonto.html')
	context = RequestContext(request, {
		'form': form
	})
	return HttpResponse(template.render(context))


@login_required
def dokoszyka(request, requested_id):
	if request.method == 'POST':
		prod = Produkt.objects.get(id=requested_id)
		try:
			kosz = Koszyk.objects.get(uzytkownik=request.user)
		except Koszyk.DoesNotExist:
			kosz = Koszyk.objects.create(uzytkownik=request.user)

		try:
			wkoszyku = ProduktKoszykowy.objects.get(koszyk=kosz, produkt=prod)
			wkoszyku.ile += 1
			wkoszyku.save()
		except ProduktKoszykowy.DoesNotExist:
			ProduktKoszykowy.objects.create(koszyk=kosz, produkt=prod, ile=1)

		return redirect('produkt', requested_id=requested_id)

	return HttpResponse()

@login_required
def koszyk(request):
	try:
		kosz = Koszyk.objects.get(uzytkownik=request.user)
	except Koszyk.DoesNotExist:
		kosz = Koszyk.objects.create(uzytkownik=request.user)

	template = loader.get_template('sklep/koszyk.html')
	context = RequestContext(request, {
		'kosz': kosz
	})
	return HttpResponse(template.render(context))


class KupzeForm(forms.Form):
    imie = forms.CharField()
    nazwisko = forms.CharField()
    adres = forms.CharField()


@login_required
def kupze(request):
	try:
		kosz = Koszyk.objects.get(uzytkownik=request.user)
	except Koszyk.DoesNotExist:
		kosz = Koszyk.objects.create(uzytkownik=request.user)

	if request.method == 'POST':
		form = KupzeForm(request.POST)
		if form.is_valid():
			ProduktKoszykowy.objects.filter(koszyk=kosz).delete()
			return HttpResponseRedirect(reverse('kupione'))
	else:
		form = KupzeForm()

	template = loader.get_template('sklep/kupze.html')
	context = RequestContext(request, {
		'kosz': kosz,
		'form': form
	})
	return HttpResponse(template.render(context))


@login_required
def kupione(request):
	template = loader.get_template('sklep/kupione.html')
	context = RequestContext(request)
	return HttpResponse(template.render(context))