from sklep.models import Kategoria

def parsuj_kategorie(rodzic, wynik, poziom):
	for kat in rodzic.kategoria_set.all():
		wynik.append((poziom, kat))
		parsuj_kategorie(kat, wynik, poziom + 1)

def kategorie(request):
	kategoria_list = Kategoria.objects.get(parrent=None)

	wynik = [(0, kategoria_list)]

	parsuj_kategorie(kategoria_list, wynik, 1)

	return {
		'kategoria_list': wynik,
	}
