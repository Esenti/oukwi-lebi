from django.conf.urls import patterns, url

from sklep import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
	url(r'^kojot/$', views.kojot, name='kojot'),
    url(r'^(?P<requested_id>\d+)/$', views.kategoria, name='kategoria'),
    url(r'^produkty/(?P<requested_id>\d+)/$', views.produkt, name='produkt'),
    url(r'^produkty/(?P<requested_id>\d+)/dokoszyka$', views.dokoszyka, name='dokoszyka'),
	url(r'^kontakt/$', views.kontakt, name='kontakt'),
	url(r'^osklepie/$', views.osklepie, name='osklepie'),
	url(r'^zaloguj/$', views.zaloguj, name='zaloguj'),
	url(r'^nowekonto/$', views.nowekonto, name='nowekonto'),
	url(r'^koszyk/$', views.koszyk, name='koszyk'),
	url(r'^kupze/$', views.kupze, name='kupze'),
	url(r'^kupione/$', views.kupione, name='kupione'),
	url(r'^wyloguj/$', 'django.contrib.auth.views.logout', { 'next_page': '/sklep' }),
)
