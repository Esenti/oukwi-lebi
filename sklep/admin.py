from django.contrib import admin
from sklep.models import Kategoria
from sklep.models import Produkt

admin.site.register(Kategoria)
admin.site.register(Produkt)