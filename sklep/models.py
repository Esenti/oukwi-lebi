from django.db import models
from django.contrib.auth.models import User

class Poll(models.Model):
    question = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.question

class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __unicode__(self):
        return self.choice_text

class Kategoria(models.Model):
    id = models.AutoField(primary_key=True)
    level = models.IntegerField(default=0)
    nazwa_kat = models.CharField('nazwa kategorii',max_length=100)
    parrent = models.ForeignKey('self', null=True)
    def __unicode__(self):
        return self.nazwa_kat

class Produkt(models.Model):
    nazwa_prod = models.CharField('nazwa produktu',max_length=100)
    opis_prod = models.CharField('opis produktu',max_length=300)
    kategoria = models.ForeignKey(Kategoria)
    cena = models.IntegerField(default=0)
    def __unicode__(self):
        return self.nazwa_prod

class Koszyk(models.Model):
    produkty = models.ManyToManyField(Produkt, through='ProduktKoszykowy')
    uzytkownik = models.OneToOneField(User, related_name='koszyk')

class ProduktKoszykowy(models.Model):
    produkt = models.ForeignKey(Produkt)
    koszyk = models.ForeignKey(Koszyk)
    ile = models.IntegerField()
